describe("Sign Up Form", () => {
  it("should successfully submit the form", () => {
    cy.visit("http://localhost:3000/");

    // Fill in the form fields
    cy.get('input[name="firstName"]').type("John");
    cy.get('input[name="lastName"]').type("Doe");
    cy.get('input[name="email"]').type("john@example.com");
    cy.get('input[name="password"]').type("Password123");

    // Here it goes
    cy.contains("Sign up").click();

    // Check if submit succeeded
    cy.contains("Thanks for signing up!");

    // Add an extra second, just in case, for waiting the 4s
    cy.wait(5000);

    // Should have the fetched data on the page now
    cy.contains("Fetched data:");
  });

  it("should show required errors if submitted without data", () => {
    cy.visit("http://localhost:3000/");

    // Here it goes
    cy.contains("Sign up").click();

    cy.contains("First name is required");
    cy.contains("Last name is required");
    cy.contains("Email is required");
    cy.contains("Password is required");
  });

  it("should show an error if password is not meeting requirements", () => {
    cy.visit("http://localhost:3000/");

    // Fill in the form fields
    cy.get('input[name="firstName"]').type("John");
    cy.get('input[name="lastName"]').type("Doe");
    cy.get('input[name="email"]').type("john@example.com");
    cy.get('input[name="password"]').type("pass");

    // Here it goes
    cy.contains("Sign up").click();

    cy.contains("Password should be a minimum of 8 characters");

    cy.get('input[name="password"]').type("password123");

    cy.contains("Sign up").click();

    cy.contains("Password should contain lower and uppercase letters");
  });

  it("should show an error if password contains first or last name", () => {
    cy.visit("http://localhost:3000/");

    // Fill in the form fields
    cy.get('input[name="firstName"]').type("John");
    cy.get('input[name="lastName"]').type("Doe");
    cy.get('input[name="password"]').type("JohnDoe1");

    // Here it goes
    cy.contains("Sign up").click();

    cy.contains("Password should not contain user's first or last name");

    cy.get('input[name="password"]').type("John1234");

    cy.contains("Password should not contain user's first or last name");

    cy.get('input[name="password"]').type("Doe12345");

    cy.contains("Password should not contain user's first or last name");
  });
});
