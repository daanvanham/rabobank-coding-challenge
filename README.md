# README

This is the code for the coding challenge for Rabobank. My estimation on parts of this application can be found in the `ESTIMATION.md` file.

## Getting Started

First, run the development server:

```bash
npm run dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the sign-up page.

## Testing

There are Cypress tests and Jest tests in the repository.

### Jest

To check the Jest tests, simply run this command:

```bash
npm run test
```

And to check the test coverage:

```bash
npm run test:coverage
```

### Cypress

To check the end-to-end tests with Cypress, spinning up the local environment is required. So run that in one terminal and spin up Cypress in another with this command:

```bash
npm run test:e2e
```

## Build

Building the application is fairly easy, just run this command:

```bash
npm run build
```

Running the builded code is done with:

```bash
npm run start
```

## Next steps

What I would've added/changed if I had more time for this feature to be build

- I don't really like the hardcoded "first/last name" error message in the PasswordValidator. I would prefer to extend the `matches` in there to be able to set custom messages for each field to check
- I wasn't sure what to do with the fetched data after the 4s, so I just placed the JSON on the page for now
- Using fixtures for the Cypress test instead of the text in the test file now
- Maybe use React Form Hook instead of the way I'm setting up the form now, but for this smaller project it seemed nicer to do it like this
- Add key event for keyCode 13 to submit form without clicking the button
- Make it look nicer :)
