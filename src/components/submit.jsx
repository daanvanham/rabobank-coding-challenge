import { LoadingButton } from "@mui/lab";

export default function Submit({ loading, label, handleClick }) {
  return (
    <LoadingButton
      loading={loading}
      variant="outlined"
      disabled={loading}
      onClick={handleClick}
      sx={{ marginTop: "1rem" }}
    >
      {label}
    </LoadingButton>
  );
}
