import { TextField } from "@mui/material";

export default function Field({ error, name, value, label, type, onChange }) {
  return (
    <TextField
      fullWidth
      name={name}
      label={label}
      value={value}
      type={type}
      onChange={onChange}
      error={!!error}
      helperText={error}
      variant="standard"
      margin="dense"
      sx={{ display: "block", width: "30rem" }}
    />
  );
}
