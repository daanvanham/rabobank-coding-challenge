import { useEffect, useState } from "react";

import Field from "~/components/field";
import Submit from "~/components/submit";

import {
  validateEmail,
  validateFirstName,
  validateLastName,
  validatePassword,
} from "~/tools/validators";

export default function Form({ onSubmit }) {
  const [pending, setPending] = useState(false);
  const [loading, setLoading] = useState(false);
  const [firstName, setFirstName] = useState("");
  const [firstNameError, setFirstNameError] = useState("");
  const [lastName, setLastName] = useState("");
  const [lastNameError, setLastNameError] = useState("");
  const [email, setEmail] = useState("");
  const [emailError, setEmailError] = useState("");
  const [password, setPassword] = useState("");
  const [passwordError, setPasswordError] = useState("");

  useEffect(() => {
    async function submit() {
      if (loading && !pending) {
        setPending(true);

        if (
          firstNameError === "" &&
          lastNameError === "" &&
          emailError === "" &&
          passwordError === ""
        ) {
          await onSubmit({ firstName, lastName, email, password });
        }

        setPending(false);
        setLoading(false);
      }
    }

    submit();
  }, [
    firstName,
    lastName,
    email,
    password,
    firstNameError,
    lastNameError,
    emailError,
    passwordError,
    onSubmit,
    loading,
    pending,
  ]);

  const handleClick = async () => {
    setLoading(true);
    setFirstNameError(validateFirstName(firstName));
    setLastNameError(validateLastName(lastName));
    setEmailError(validateEmail(email));
    setPasswordError(validatePassword(password, [firstName, lastName]));
  };

  return (
    <form>
      <Field
        name="firstName"
        value={firstName}
        label="First name"
        error={firstNameError}
        onChange={(event) => {
          setFirstNameError("");
          setFirstName(event.target.value);
        }}
      />

      <Field
        name="lastName"
        value={lastName}
        label="Last name"
        error={lastNameError}
        onChange={(event) => {
          setLastNameError("");
          setLastName(event.target.value);
        }}
      />

      <Field
        type="email"
        name="email"
        value={email}
        label="Email"
        error={emailError}
        onChange={(event) => {
          setEmailError("");
          setEmail(event.target.value);
        }}
      />

      <Field
        type="password"
        name="password"
        value={password}
        label="Password"
        error={passwordError}
        onChange={(event) => {
          setPasswordError;
          setPassword(event.target.value);
        }}
      />

      <Submit label="Sign up" loading={loading} handleClick={handleClick} />
    </form>
  );
}
