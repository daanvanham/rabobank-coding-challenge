export function validateEmail(email) {
  if (email === "") {
    return "Email is required";
  }

  if (!/^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email)) {
    return "Email is invalid";
  }

  return "";
}

export function validateFirstName(firstName) {
  if (firstName === "") {
    return "First name is required";
  }

  if (firstName.length < 2 || firstName.length > 50) {
    return "First name should be between 2 and 50 characters";
  }

  if (!/^[a-zA-Z\s'-]+$/.test(firstName)) {
    return "First name contains invalid characters";
  }

  return "";
}

export function validateLastName(lastName) {
  if (lastName === "") {
    return "Last name is required";
  }

  if (lastName.length < 2 || lastName.length > 50) {
    return "Last name should be between 2 and 50 characters";
  }

  if (!/^[a-zA-Z\s'-]+$/.test(lastName)) {
    return "Last name contains invalid characters";
  }

  return "";
}

export function validatePassword(password, matches = []) {
  if (password === "") {
    return "Password is required";
  }

  if (password.length < 8) {
    return "Password should be a minimum of 8 characters";
  }

  if (!/[a-z]/.test(password) || !/[A-Z]/.test(password)) {
    return "Password should contain lower and uppercase letters";
  }

  for (let challenge of matches) {
    if (password.toLowerCase().includes(challenge.toLowerCase())) {
      return "Password should not contain user's first or last name";
    }
  }

  return "";
}
