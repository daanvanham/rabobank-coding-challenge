import {
  validateEmail,
  validateFirstName,
  validateLastName,
  validatePassword,
} from "~/tools/validators";

describe("validateEmail", () => {
  it('returns "Email is required" when email is empty', () => {
    const email = "";
    const result = validateEmail(email);

    expect(result).toBe("Email is required");
  });

  it('returns "Email is invalid" when email is not in a valid format', () => {
    const email = "invalidemail";
    const result = validateEmail(email);

    expect(result).toBe("Email is invalid");
  });

  it("returns an empty string when email is valid", () => {
    const email = "test@example.com";
    const result = validateEmail(email);

    expect(result).toBe("");
  });
});

describe("validateFirstName", () => {
  it('returns "First name is required" when first name is empty', () => {
    const firstName = "";
    const result = validateFirstName(firstName);
    expect(result).toBe("First name is required");
  });

  it('returns "First name should be between 2 and 50 characters" when first name length is less than 2', () => {
    const firstName = "A";
    const result = validateFirstName(firstName);
    expect(result).toBe("First name should be between 2 and 50 characters");
  });

  it('returns "First name should be between 2 and 50 characters" when first name length is greater than 50', () => {
    const firstName =
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed.";
    const result = validateFirstName(firstName);
    expect(result).toBe("First name should be between 2 and 50 characters");
  });

  it('returns "First name contains invalid characters" when first name contains special characters', () => {
    const firstName = "John@Doe";
    const result = validateFirstName(firstName);
    expect(result).toBe("First name contains invalid characters");
  });

  it("returns an empty string when first name is valid", () => {
    const firstName = "John";
    const result = validateFirstName(firstName);
    expect(result).toBe("");
  });
});

describe("validateLastName", () => {
  it('returns "Last name is required" when last name is empty', () => {
    const lastName = "";
    const result = validateLastName(lastName);
    expect(result).toBe("Last name is required");
  });

  it('returns "Last name should be between 2 and 50 characters" when last name length is less than 2', () => {
    const lastName = "A";
    const result = validateLastName(lastName);
    expect(result).toBe("Last name should be between 2 and 50 characters");
  });

  it('returns "Last name should be between 2 and 50 characters" when last name length is greater than 50', () => {
    const lastName =
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed.";
    const result = validateLastName(lastName);
    expect(result).toBe("Last name should be between 2 and 50 characters");
  });

  it('returns "Last name contains invalid characters" when last name contains special characters', () => {
    const lastName = "John@Doe";
    const result = validateLastName(lastName);
    expect(result).toBe("Last name contains invalid characters");
  });

  it("returns an empty string when last name is valid", () => {
    const lastName = "John";
    const result = validateLastName(lastName);
    expect(result).toBe("");
  });
});

describe("validatePassword", () => {
  it('returns "Password is required" when password is empty', () => {
    const password = "";
    const result = validatePassword(password);
    expect(result).toBe("Password is required");
  });

  it('returns "Password should be a minimum of 8 characters" when password length is less than 8', () => {
    const password = "pass";
    const result = validatePassword(password, ["John"]);
    expect(result).toBe("Password should be a minimum of 8 characters");
  });

  it('returns "Password should contain lower and uppercase letters" when password does not contain both lower and uppercase letters', () => {
    const password = "password";
    const result = validatePassword(password, ["John"]);
    expect(result).toBe("Password should contain lower and uppercase letters");
  });

  it("returns \"Password should not contain user's first or last name\" when password contains user's first name", () => {
    const password = "JohnDoe123";
    const matches = ["John", "Doe"];
    const result = validatePassword(password, matches);

    expect(result).toBe(
      "Password should not contain user's first or last name"
    );
  });

  it("returns an empty string when password is valid", () => {
    const password = "SuperStrongPassword123";
    const result = validatePassword(password, ["John"]);
    expect(result).toBe("");
  });
});
