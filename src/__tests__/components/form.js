import "@testing-library/jest-dom";

import React from "react";
import { render, fireEvent, waitFor } from "@testing-library/react";
import Form from "~/components/form";

describe("Form", () => {
  it("submits the form when all fields are valid", async () => {
    const mockSubmit = jest.fn();

    const { getByLabelText, getByText } = render(
      <Form onSubmit={mockSubmit} />
    );

    // Fill in the form fields
    fireEvent.change(getByLabelText("First name"), {
      target: { value: "John" },
    });
    fireEvent.change(getByLabelText("Last name"), { target: { value: "Doe" } });
    fireEvent.change(getByLabelText("Email"), {
      target: { value: "test@example.com" },
    });
    fireEvent.change(getByLabelText("Password"), {
      target: { value: "Password123" },
    });

    // Submit the form
    fireEvent.click(getByText("Sign up"));

    // Verify that the onSubmit function was called
    await waitFor(() => {
      expect(mockSubmit).toHaveBeenCalledTimes(1);
      expect(mockSubmit).toHaveBeenCalledWith({
        firstName: "John",
        lastName: "Doe",
        email: "test@example.com",
        password: "Password123",
      });
    });
  });

  it("displays field errors when invalid data is entered", async () => {
    const mockSubmit = jest.fn();

    const { getByLabelText, getByText, queryByText } = render(
      <Form onSubmit={mockSubmit} />
    );

    // Fill in the form fields with invalid data
    fireEvent.change(getByLabelText("First name"), { target: { value: "" } });
    fireEvent.change(getByLabelText("Last name"), { target: { value: "" } });
    fireEvent.change(getByLabelText("Email"), {
      target: { value: "invalid-email" },
    });
    fireEvent.change(getByLabelText("Password"), {
      target: { value: "short" },
    });

    // Submit the form
    fireEvent.click(getByText("Sign up"));

    // Verify that field errors are displayed
    expect(getByText("First name is required")).toBeInTheDocument();
    expect(getByText("Last name is required")).toBeInTheDocument();
    expect(getByText("Email is invalid")).toBeInTheDocument();
    expect(
      getByText("Password should be a minimum of 8 characters")
    ).toBeInTheDocument();

    // Verify that the onSubmit function was not called
    await waitFor(() => {
      expect(mockSubmit).not.toHaveBeenCalled();
    });

    // Clear the errors
    fireEvent.change(getByLabelText("First name"), {
      target: { value: "John" },
    });
    fireEvent.change(getByLabelText("Last name"), { target: { value: "Doe" } });
    fireEvent.change(getByLabelText("Email"), {
      target: { value: "test@example.com" },
    });
    fireEvent.change(getByLabelText("Password"), {
      target: { value: "Password123" },
    });

    // Submit the form
    fireEvent.click(getByText("Sign up"));

    await waitFor(() => {
      // Verify that the errors are cleared
      expect(queryByText("First name is required")).toBeNull();
      expect(queryByText("Last name is required")).toBeNull();
      expect(queryByText("Email is invalid")).toBeNull();
      expect(
        queryByText("Password should be a minimum of 8 characters")
      ).toBeNull();
    });
  });

  it("displays error message for password field containing first and/or last name", async () => {
    const mockSubmit = jest.fn();

    const { getByLabelText, getByText, queryByText } = render(
      <Form onSubmit={mockSubmit} />
    );

    // Fill in the form fields with invalid data
    fireEvent.change(getByLabelText("First name"), {
      target: { value: "John" },
    });
    fireEvent.change(getByLabelText("Last name"), { target: { value: "Doe" } });
    fireEvent.change(getByLabelText("Password"), {
      target: { value: "JohnDoe1" },
    });

    // Submit the form
    fireEvent.click(getByText("Sign up"));

    // Verify that field errors are displayed
    expect(
      getByText("Password should not contain user's first or last name")
    ).toBeInTheDocument();

    // Verify that the onSubmit function was not called
    await waitFor(() => {
      expect(mockSubmit).not.toHaveBeenCalled();
    });
  });
});
