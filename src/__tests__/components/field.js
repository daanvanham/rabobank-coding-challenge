import "@testing-library/jest-dom";

import React from "react";
import { render, fireEvent } from "@testing-library/react";
import Field from "~/components/field";

describe("Field", () => {
  it("renders the field label correctly", () => {
    const label = "Username";
    const { getByLabelText } = render(<Field label={label} />);
    const fieldElement = getByLabelText(label);

    expect(fieldElement).toBeInTheDocument();
  });

  it("passes the value and name to the input element", () => {
    const name = "username";
    const value = "testuser";
    const { getByDisplayValue } = render(<Field name={name} value={value} />);
    const inputElement = getByDisplayValue(value);

    expect(inputElement).toBeInTheDocument();
    expect(inputElement.getAttribute("name")).toBe(name);
  });

  it("calls the onChange function when input value changes", () => {
    const onChangeMock = jest.fn();
    const { getByRole } = render(<Field onChange={onChangeMock} />);
    const inputElement = getByRole("textbox");
    const testValue = "test";

    fireEvent.change(inputElement, { target: { value: testValue } });

    expect(onChangeMock).toHaveBeenCalledTimes(1);
    expect(inputElement.value).toBe(testValue);
  });

  it("renders the error message correctly", () => {
    const error = "Invalid username";
    const { getByText } = render(<Field error={error} />);
    const errorElement = getByText(error);

    expect(errorElement).toBeInTheDocument();
  });
});
