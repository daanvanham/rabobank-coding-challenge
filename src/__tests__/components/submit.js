import "@testing-library/jest-dom";

import React from "react";
import { render, fireEvent } from "@testing-library/react";

import Submit from "~/components/submit";

describe("Submit", () => {
  it("renders button text correctly", () => {
    const buttonText = "Submit";
    const { getByText } = render(<Submit label={buttonText} />);
    const buttonElement = getByText(buttonText);

    expect(buttonElement).toBeInTheDocument();
  });

  it("calls the onClick function when clicked", async () => {
    const onClickMock = jest.fn();
    const { getByText } = render(
      <Submit handleClick={onClickMock} label="Submit" />
    );
    const buttonElement = getByText("Submit");

    fireEvent.click(buttonElement);

    expect(onClickMock).toHaveBeenCalledTimes(1);
  });
});
